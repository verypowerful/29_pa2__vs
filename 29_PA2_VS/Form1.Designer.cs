﻿namespace _29_PA2_VS {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.ibl_FirstNumber = new System.Windows.Forms.Label();
            this.txt_FirstNumber = new System.Windows.Forms.TextBox();
            this.Ibl_SecondNumber = new System.Windows.Forms.Label();
            this.txt_SecondNumber = new System.Windows.Forms.TextBox();
            this.btn_Add = new System.Windows.Forms.Button();
            this.btn_Quit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ibl_FirstNumber
            // 
            this.ibl_FirstNumber.AutoSize = true;
            this.ibl_FirstNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ibl_FirstNumber.Location = new System.Drawing.Point(75, 136);
            this.ibl_FirstNumber.Name = "ibl_FirstNumber";
            this.ibl_FirstNumber.Size = new System.Drawing.Size(84, 16);
            this.ibl_FirstNumber.TabIndex = 0;
            this.ibl_FirstNumber.Text = "First Number";
            // 
            // txt_FirstNumber
            // 
            this.txt_FirstNumber.Location = new System.Drawing.Point(193, 129);
            this.txt_FirstNumber.Name = "txt_FirstNumber";
            this.txt_FirstNumber.Size = new System.Drawing.Size(206, 20);
            this.txt_FirstNumber.TabIndex = 1;
            // 
            // Ibl_SecondNumber
            // 
            this.Ibl_SecondNumber.AutoSize = true;
            this.Ibl_SecondNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ibl_SecondNumber.Location = new System.Drawing.Point(75, 186);
            this.Ibl_SecondNumber.Name = "Ibl_SecondNumber";
            this.Ibl_SecondNumber.Size = new System.Drawing.Size(106, 16);
            this.Ibl_SecondNumber.TabIndex = 2;
            this.Ibl_SecondNumber.Text = "Second Number";
            // 
            // txt_SecondNumber
            // 
            this.txt_SecondNumber.Location = new System.Drawing.Point(193, 182);
            this.txt_SecondNumber.Name = "txt_SecondNumber";
            this.txt_SecondNumber.Size = new System.Drawing.Size(206, 20);
            this.txt_SecondNumber.TabIndex = 3;
            // 
            // btn_Add
            // 
            this.btn_Add.Location = new System.Drawing.Point(78, 288);
            this.btn_Add.Name = "btn_Add";
            this.btn_Add.Size = new System.Drawing.Size(144, 51);
            this.btn_Add.TabIndex = 4;
            this.btn_Add.Text = "ADD";
            this.btn_Add.UseVisualStyleBackColor = true;
            this.btn_Add.Click += new System.EventHandler(this.btn_Add_Click);
            // 
            // btn_Quit
            // 
            this.btn_Quit.Location = new System.Drawing.Point(420, 288);
            this.btn_Quit.Name = "btn_Quit";
            this.btn_Quit.Size = new System.Drawing.Size(131, 51);
            this.btn_Quit.TabIndex = 5;
            this.btn_Quit.Text = "QUIT";
            this.btn_Quit.UseVisualStyleBackColor = true;
            this.btn_Quit.Click += new System.EventHandler(this.btn_Quit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(146, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(317, 25);
            this.label1.TabIndex = 6;
            this.label1.Text = "Simple Arithmetic Operations";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 422);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_Quit);
            this.Controls.Add(this.btn_Add);
            this.Controls.Add(this.txt_SecondNumber);
            this.Controls.Add(this.Ibl_SecondNumber);
            this.Controls.Add(this.txt_FirstNumber);
            this.Controls.Add(this.ibl_FirstNumber);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ibl_FirstNumber;
        private System.Windows.Forms.TextBox txt_FirstNumber;
        private System.Windows.Forms.Label Ibl_SecondNumber;
        private System.Windows.Forms.TextBox txt_SecondNumber;
        private System.Windows.Forms.Button btn_Add;
        private System.Windows.Forms.Button btn_Quit;
        private System.Windows.Forms.Label label1;
    }
}

